'use strict'

module.exports = function makeImportant(text, numberExclamation = text.length) {
    return text + '!'.repeat(numberExclamation)
};
