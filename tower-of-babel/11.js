'use strict';

var inputs = process.argv.slice(2);
var result = inputs.map((x) => x[0])
    .reduce((r, a) => r + a[0]);
console.log(result);
