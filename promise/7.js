'use strict';

var firstPromise = new first();
var secondPromise = firstPromise.then(function(value) {
   return second(value);
});

secondPromise.then(console.log);
