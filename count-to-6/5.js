'use strict'

function prepareInput(...input) {
    return input[0].slice(2);
}

var numbers = prepareInput(process.argv);
console.log(`The minimum of [${numbers}] is ${Math.min(...numbers)}`);
