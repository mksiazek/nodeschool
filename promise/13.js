'use strict';

var http = require('q-io/http');

http.read('http://localhost:7000').then(function(response) {
    var id = response.toString();
    return http.read('http://localhost:7001/' + id);
}).then(function(response) {
    console.log(JSON.parse(response.toString()));
}).then(null, function(err) {
    console.log(err);
}).done();
