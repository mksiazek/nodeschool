'use strict'

module.exports = function average(...args) {
    return args.reduce((prev, next) => (prev + next)) / args.length;
};
