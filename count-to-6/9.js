'use strict'

console.log(html`<b>${process.argv[2]} says</b>: "${process.argv[3]}"`);

function html([a, b, c], name, comment) {
    comment = comment
        .replace('&', '&amp;')
        .replace('\'', '&apos;')
        .replace('\"', '&quot;')
        .replace('<', '&lt;')
        .replace('>', '&gt;');

    return a + name + b + comment + c;
}
