'use strict';

var koa = require('koa');
var session = require('koa-session');

var app = koa();
app.keys = ['secret', 'keys'];
app.use(session(app));

app.use(function * (next) {
    yield next;

    this.body = `${this.views} views`
});

app.use(function * (next) {
    this.views = this.session.views || 0;
    this.views++;

    yield next;

    this.session.views = this.views;
});

app.listen(process.argv[2]);
