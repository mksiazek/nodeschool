'use strict'

var name = process.argv[2];
var greetings = `Hello, ${name}!
Your name lowercased is "${name.toLowerCase()}".`;

console.log(greetings);
