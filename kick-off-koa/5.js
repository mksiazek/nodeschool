'use strict';

var koa = require('koa');
var parse = require('co-body');
var fs = require('fs');

var app = koa();
var port = process.argv[2];

app.use(function * (next) {
    var output = 'ok';

    if(this.request.is('application/json')) {
        output = { message: "hi!" };
    }

    this.body = output;
});

app.listen(port);
