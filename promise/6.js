'use strict';

function onReject(error) {
    console.log(error.message);
}

var promise = new Promise(function(fulfill) {
    fulfill('PROMISE VALUE');
});

promise.then(console.log);

promise = Promise.resolve('PROMISE VALUE');
promise.then(console.log);

promise = new Promise(function(fulfill, reject) {
    reject(new Error('REJECTED'));
});
promise.then(null, onReject);

promise = Promise.reject(new Error('REJECTED'));
promise.catch(onReject);
