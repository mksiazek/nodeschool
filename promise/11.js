'use strict';

function all(param1, param2) {
    return new Promise(function(fulfill) {
        var counter = 0;
        var out = [];

        param1.then((value) => {
            out[0] = value;
            counter++;

            if(counter >= 2) {
                fulfill(out);
            }
        });

        param2.then((value) => {
            out[1] = value;
            counter++;

            if(counter >= 2) {
                fulfill(out);
            }
        });
    });
}

all(getPromise1(), getPromise2())
    .then(console.log);
