'use strict';

function parsePromised(value) {
    return new Promise(function(fulfill, rejected) {
        try {
            var parsed = JSON.parse(value);
            fulfill(parsed);
        } catch(e) {
            rejected(e);
        }
    });
}

parsePromised(process.argv[2])
    .then(null, console.log);
