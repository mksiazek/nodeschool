'use strict';

function attachTitle(name) {
    return 'DR. ' + name;
}

var promise = new Promise(function(fulfill) {
   fulfill('MANHATTAN');
});

promise
    .then(attachTitle)
    .then(console.log);
