'use strict';

var Q = require('q');

function alwaysThrows() {
    throw new Error('OH NOES');
}

function iterate(value) {
    console.log(value);
    return ++value;
}

Promise.resolve(iterate(1))
    .then(iterate)
    .then(iterate)
    .then(iterate)
    .then(iterate)
    .then(alwaysThrows)
    .then(iterate)
    .then(iterate)
    .then(iterate)
    .then(iterate)
    .then(iterate)
    .then(null, console.log);;
