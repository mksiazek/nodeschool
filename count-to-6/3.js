'use strict'

var inputs = process.argv.slice(2);
var result = inputs.map((word) => word[0])
    .reduce((prev, curr) => prev + curr);
var output = `[${inputs}] becomes "${result}"`;

console.log(output);
