'use strict';

var koa = require('koa');

var app = koa();
app.keys = ['secret', 'keys'];

app.use(function * (next) {
    yield next;

    this.body = `${this.views} views`
});

app.use(function * (next) {
    this.views = this.cookies.get('view', { signed: true }) || 0;
    this.views++;

    yield next;

    this.cookies.set('view', this.views, { signed: true })
});

app.listen(process.argv[2]);
