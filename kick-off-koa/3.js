'use strict';

var koa = require('koa');
var parse = require('co-body');
var app = koa();
var port = process.argv[2];

app.use(function * (next) {
    var body = yield parse(this);
    this.body = body.name.toUpperCase();
});

app.listen(port);
